# -*- coding: utf-8 -*-
import sys
import os
import sqlite3
from os.path import isfile, dirname, join

import qdarkstyle
from PySide.QtCore import Qt, QSize
from PySide.QtGui import (QApplication, QMessageBox, QTreeWidgetItem, QFont,
                          QColor, QBrush, QIcon, QPixmap)
from spark_assist_ui import SparkAssistUI, TechItem, UnitItem
import lib
import resource
from spark_assist import __version__


sqlite_file = lib.get_file_path()['sqlite']
current_path = dirname(__file__)


class SparkAssist(SparkAssistUI):
    def __init__(self, parent=None):
        super(SparkAssist, self).__init__(parent)
        self.setWindowTitle('RS2 Spark Assistant v{}'.format(__version__))
        if not os.path.isfile(sqlite_file):
            self.warning('Database file not found')
            return
        self.conn = sqlite3.connect(sqlite_file)
        self.cur = self.conn.cursor()
        self.init_tree()

    def init_tree(self):
        if self.techRadio.isChecked():
            self.load_tech(init=True)
        else:
            self.load_unit(init=True)

    def load_tech(self, init=False, spark_type=None):
        self.techTree.clear()
        if init:
            self.cur.execute("""
                SELECT weapon_type.name, tech.name, tech_spark_from.difficulty,
                    tech.from_weapon
                FROM tech
                    LEFT JOIN weapon_type ON tech.weapon_type_id=weapon_type.id 
                    LEFT JOIN tech_spark_from ON tech.id=tech_spark_from.tech_id
                WHERE tech_spark_from.spark_from_id is null
                    AND tech_spark_from.difficulty is not null
                ORDER BY tech_spark_from.difficulty""")
            # ORDER BY tech_spark_from.difficulty ASC
            tech_base = self.cur.fetchall()
        else:
            self.cur.execute("""
                SELECT weapon_type.name, tech.name, tech_spark_from.difficulty,
                    tech.from_weapon
                FROM spark_tech_pair p
                    LEFT JOIN tech ON p.tech_id=tech.id
                    LEFT JOIN weapon_type ON tech.weapon_type_id=weapon_type.id
                    LEFT JOIN tech_spark_from ON tech.id=tech_spark_from.tech_id
                WHERE
                    p.spark_type_id=?
                    AND tech_spark_from.spark_from_id is null
                    AND tech_spark_from.difficulty is not null
                ORDER BY tech_spark_from.difficulty
            """, (spark_type,))
            tech_base = self.cur.fetchall()
        self.cur.execute("""
            SELECT 
                tech.name AS name,
                t2.name AS spark_from,
                t.difficulty AS diff
            FROM tech
                LEFT JOIN tech_spark_from t ON tech.id=t.tech_id
                LEFT JOIN tech t2 ON t.spark_from_id=t2.id
            WHERE t.spark_from_id is not null
            """)
        tech_child = self.cur.fetchall()
        level1 = {}
        for each in tech_base:
            parent = each[0]
            name = each[1]
            difficulty = each[2]
            from_weapon = each[3]
            parents = self.techTree.findItems(parent, Qt.MatchExactly)
            if not parents:
                self.techTree.addTopLevelItem(QTreeWidgetItem([each[0]]))
                parent = self.techTree.findItems(parent, Qt.MatchExactly)[0]
                icon = QIcon(':/ico/weapon/{}.png'.format(
                        parent.text(0).replace(' ', '_')))
                parent.setIcon(0, icon)
            else:
                parent = parents[0]
            new_entry = TechItem(name=name, difficulty=difficulty, from_weapon=from_weapon)
            parent.addChild(new_entry)
            level1[name] = new_entry
        level2 = {}
        for each in tech_child:
            spark_from = each[1]
            name = each[0]
            difficulty = each[2]
            # print spark_from
            if spark_from in level1:
                new_entry = TechItem(name=name, difficulty=difficulty)
                level1[spark_from].addChild(new_entry)
                if name not in level2:
                    level2[name] = []
                level2[name].append(new_entry)
        # 3rd level is the deepest level. Don't want to get into recursive
        # loop so I just repeat the above loop
        for each in tech_child:
            spark_from = each[1]
            name = each[0]
            difficulty = each[2]
            # print spark_from
            if spark_from in level2:
                for each_item in level2[spark_from]:
                    new_entry = TechItem(name=name, difficulty=difficulty)
                    each_item.addChild(new_entry)

    def load_unit(self, init=False, tech_name=None):
        self.unitTree.clear()
        if init:
            self.cur.execute("""
                SELECT unit.name, unit.is_female, unit_class.name, unit.spark_type_id
                FROM unit
                LEFT JOIN unit_class ON unit.unit_class_id=unit_class.id
                ORDER BY unit_class.name, unit.is_female""")
            result = self.cur.fetchall()
        else:
            self.cur.execute("""
                SELECT 
                    unit.name, unit.is_female, unit_class.name, unit.spark_type_id
                FROM tech
                    LEFT JOIN spark_tech_pair p ON tech.id=p.tech_id
                    LEFT JOIN unit ON p.spark_type_id=unit.spark_type_id
                    LEFT JOIN unit_class ON unit.unit_class_id=unit_class.id 
                WHERE tech.name = ? 
                ORDER BY unit_class.name, unit.is_female
            """, (tech_name,))
            result = self.cur.fetchall()
        for each in result:
            name = each[0]
            if name is None:
                continue
            is_female = each[1]
            unit_class = each[2]
            spark_type = each[3]
            # if is_female:
            #     unit_class += ' F'
            # else:
            #     unit_class += ' M'
            parents = self.unitTree.findItems(unit_class, Qt.MatchExactly)
            if not parents:
                self.unitTree.addTopLevelItem(QTreeWidgetItem([unit_class]))
                parent = self.unitTree.findItems(unit_class, Qt.MatchExactly)[0]
            else:
                parent = parents[0]
            new_entry = UnitItem(name=name, spark_type=spark_type)
            icon = QIcon(':/ico/unit/{}.png'.format(name.replace(' ', '_')))
            new_entry.setIcon(0, icon)
            parent.addChild(new_entry)

    def load_monster(self, difficulty=None):
        self.monsterTree.clear()
        if not difficulty:
            return
        difficulty = int(difficulty)
        low_end = difficulty - 5
        high_end = difficulty + 6
        # Prepare parent items
        diffs = [
            '0.39% (-5)',
            '0.78% (-4)',
            '1.57% (-3)',
            '2.35% (-2)',
            '5.1% (-1)',
            '20.4% **',
            '7.84% (+1)',
            '8.63% (+2)',
            '8.63% (+3)',
            '9.41% (+4)',
            '9.41% (+5)',
            '9.8% (+6)'
        ]
        bold = QFont()
        bold.setBold(True)
        diff_items = []
        for i, each in enumerate(diffs):
            new_entry = QTreeWidgetItem([each])
            i -= 5
            if i == 0:
                new_entry.setForeground(0, QBrush(Qt.yellow))
                new_entry.setFont(0, bold)
                # new_entry.setExpanded(True)
            elif i <= -2:
                new_entry.setForeground(0, QBrush(QColor(100, 100, 100)))
                pass
            else:
                new_entry.setExpanded(True)
            self.monsterTree.addTopLevelItem(new_entry)
            diff_items.append(new_entry)

        self.cur.execute("""
            SELECT
                monster.name, monster.tech_level, m.name
            FROM monster
                LEFT JOIN monster_class m ON monster.monster_class_id=m.id
            WHERE 
                monster.tech_level >= ? AND
                monster.tech_level <= ?
            ORDER BY monster.tech_level
        """, (low_end, high_end))
        result = self.cur.fetchall()
        bold = QFont()
        bold.setBold(True)
        for each in result:
            name = each[0]
            diff = each[1] - difficulty
            monster_class = each[2]
            index = diff + 5

            new_entry = QTreeWidgetItem(['{} ({})'.format(name, monster_class)])
            if diff == 0:
                new_entry.setForeground(0, QBrush(Qt.yellow))
            icon = QIcon(':/ico/monster/{}.gif'.format(name.replace(' ', '_')))
            if not icon.pixmap(QSize(64, 64)).isNull():
                img = QTreeWidgetItem()
                img.setIcon(0, icon)
                new_entry.addChild(img)
            diff_items[index].addChild(new_entry)
            if diff >= 0:
                diff_items[index].setExpanded(True)

    def change_tech(self, current, previous):
        if not isinstance(current, TechItem):
            return
        if self.techRadio.isChecked():
            # load units too.
            self.unitTree.clear()
            self.load_unit(tech_name=current.name)
        self.monsterTree.clear()
        self.load_monster(difficulty=current.difficulty)

    def change_unit(self, current, previous):
        if not isinstance(current, UnitItem):
            return
        if self.techRadio.isChecked():
            return
        self.techTree.clear()
        self.load_tech(spark_type=current.spark_type)

    def warning(self, message):
        QMessageBox.warning(self, 'Warning', message)


if __name__ == '__main__':
    # Test launch GUI
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet())
    spark_assist = SparkAssist()
    spark_assist.show()
    spark_assist.raise_()
    sys.exit(app.exec_())
