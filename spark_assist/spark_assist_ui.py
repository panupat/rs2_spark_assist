# -*- coding: utf-8 -*-
import sys
import qdarkstyle
from PySide.QtCore import QSize
from PySide.QtGui import (QWidget, QApplication, QVBoxLayout, QSplitter,
                          QTreeWidget,
                          QTreeWidgetItem, QFont, QHBoxLayout, QRadioButton,
                          QHeaderView, QIcon)
import resource


class SparkAssistUI(QWidget):
    def __init__(self, parent=None):
        super(SparkAssistUI, self).__init__(parent)
        self.setWindowTitle('RS2 Tech Spark Assistant')
        self.setWindowIcon(QIcon(':/ico/unit/Gerard_(armored)'))
        self.setMinimumHeight(500)
        self.setMinimumWidth(400)
        font = QFont()
        font.setPointSize(10)
        # row1
        row1 = QHBoxLayout()
        self.techRadio = QRadioButton('from Tech')
        self.techRadio.setChecked(True)
        self.unitRadio = QRadioButton('from Character')
        row1.addWidget(self.techRadio)
        row1.addWidget(self.unitRadio)
        row1.addStretch()
        # row #2
        self.row2 = QSplitter()
        # self.row2 = QHBoxLayout()
        self.techTree = QTreeWidget()
        self.techTree.setFont(font)
        self.techTree.setHeaderLabel('Tech')
        self.techTree.setEditTriggers(QTreeWidget.NoEditTriggers)
        self.techTree.setSelectionMode(QTreeWidget.SingleSelection)
        self.techTree.header().setResizeMode(QHeaderView.ResizeToContents)
        self.techTree.header().setStretchLastSection(False)
        self.techTree.setIndentation(15)

        self.unitTree = QTreeWidget()
        self.unitTree.setFont(font)
        self.unitTree.setHeaderLabel('Character')
        self.unitTree.setEditTriggers(QTreeWidget.NoEditTriggers)
        self.unitTree.setSelectionMode(QTreeWidget.SingleSelection)
        self.unitTree.header().setResizeMode(QHeaderView.ResizeToContents)
        self.unitTree.header().setStretchLastSection(False)
        self.unitTree.setIconSize(QSize(30, 30))
        self.unitTree.setIndentation(10)

        self.monsterTree = QTreeWidget()
        self.monsterTree.setFont(font)
        self.monsterTree.setHeaderLabel('Spark % / Monster')
        self.monsterTree.setEditTriggers(QTreeWidget.NoEditTriggers)
        self.monsterTree.setSelectionMode(QTreeWidget.ExtendedSelection)
        self.monsterTree.setIconSize(QSize(200, 200))

        self.row2.addWidget(self.techTree)
        self.row2.addWidget(self.unitTree)
        self.row2.addWidget(self.monsterTree)

        # LAYOUT Main
        layout = QVBoxLayout()
        self.setLayout(layout)
        layout.addLayout(row1)
        layout.addWidget(self.row2)
        # layout.addLayout(self.row2)
        # Signal
        self.techRadio.toggled.connect(self.change_radio)
        self.techTree.currentItemChanged.connect(self.change_tech)
        self.unitTree.currentItemChanged.connect(self.change_unit)

    def change_radio(self, is_tech):
        self.techTree.clear()
        self.unitTree.clear()
        self.monsterTree.clear()
        widget = QWidget()
        self.techTree.setParent(widget)
        self.unitTree.setParent(widget)
        self.monsterTree.setParent(widget)
        if is_tech:
            self.row2.addWidget(self.techTree)
            self.row2.addWidget(self.unitTree)
        else:
            self.row2.addWidget(self.unitTree)
            self.row2.addWidget(self.techTree)
        self.row2.addWidget(self.monsterTree)
        widget.deleteLater()
        self.row2.setSizes((100, 100, 100))
        self.init_tree()

    def init_tree(self):
        pass

    def change_tech(self, current, previous):
        pass

    def change_unit(self, current, previos):
        pass

class TechItem(QTreeWidgetItem):
    def __init__(self, name, difficulty=0, from_weapon=None, *args, **kwargs):
        super(TechItem, self).__init__(*args, **kwargs)
        self.name = name
        self.difficulty = difficulty
        # self.weapon = weapon
        text = '[{}] {}'.format(difficulty, name)
        if from_weapon:
            text += ' - {}'.format(from_weapon)
        self.setText(0, text)


class UnitItem(QTreeWidgetItem):
    def __init__(self, name, spark_type, *args, **kwargs):
        super(UnitItem, self).__init__(*args, **kwargs)
        self.spark_type = spark_type
        self.setText(0, name)


if __name__ == '__main__':
    # Test launch GUI
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet())
    SPUI = SparkAssistUI()

    item1 = TechItem(difficulty=10, name='Slice')
    item2 = TechItem(difficulty=20, name='Final Strike', weapon='Goblin Sword')
    item3 = TechItem(difficulty=30, name='Final Strike', weapon='Xcalipur')
    item1.addChild(item2)
    SPUI.techTree.addTopLevelItem(item1)
    SPUI.techTree.addTopLevelItem(item2)
    SPUI.techTree.addTopLevelItem(item3)

    SPUI.show()
    SPUI.raise_()

    sys.exit(app.exec_())