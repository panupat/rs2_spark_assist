# -*- coding: utf-8 -*-
"""
These are functions for generating the SQLite database. Source data were
based on Elliot20's spreadsheet saved as CSV. While there are some manual
inputs, I tried to stick to CSV as much as possib le so they could be updated
easily.
"""
import os
import sqlite3
import csv
import lib
import errno


filepath = lib.get_file_path()


def init_connection(sqlite_file=filepath['sqlite']):
    return sqlite3.connect(sqlite_file)


def create_table(sqlite_file):
    conn = init_connection(sqlite_file)
    cur = conn.cursor()

    cur.execute("""
            CREATE TABLE weapon_type (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            UNIQUE(name) ON CONFLICT IGNORE
        )""")
    cur.execute("""
            CREATE TABLE tech (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            weapon_type_id INTEGER NOT NULL,
            from_weapon VARCHAR(64),
            UNIQUE(name) ON CONFLICT IGNORE,
            FOREIGN KEY(weapon_type_id) REFERENCES weapon_type(id)
        )""")
    cur.execute("""
            CREATE TABLE spark_type (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            UNIQUE(name) ON CONFLICT IGNORE
        )""")
    cur.execute("""
            CREATE TABLE spark_tech_pair (
            spark_type_id INTEGER NOT NULL,
            tech_id INTEGER NOT NULL,
            FOREIGN KEY(spark_type_id) REFERENCES spark_type(id),
            FOREIGN KEY(tech_id) REFERENCES tech(id)
        )""")
    cur.execute("""
            CREATE TABLE tech_spark_from (
            tech_id INTEGER NOT NULL,
            spark_from_id INTEGER,
            difficulty INTEGER,
            FOREIGN KEY(tech_id) REFERENCES tech(id),
            FOREIGN KEY(spark_from_id) REFERENCES tech(id)
        )""")
    cur.execute("""
            CREATE TABLE unit_class (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            UNIQUE(name) ON CONFLICT IGNORE
        )""")
    cur.execute("""
            CREATE TABLE unit (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            is_female INTEGER(2) DEFAULT 0,
            spark_type_id INTEGER,
            unit_class_id INTEGER,
            UNIQUE(name) ON CONFLICT IGNORE,
            FOREIGN KEY(spark_type_id) REFERENCES spark_type(id),
            FOREIGN KEY(unit_class_id) REFERENCES unit_class(id)
        )""")
    cur.execute("""
            CREATE TABLE monster_class (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            UNIQUE(name) ON CONFLICT IGNORE
        )""")
    cur.execute("""
            CREATE TABLE monster (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(64) NOT NULL,
            tech_level INTEGER,
            monster_class_id INTEGER,
            UNIQUE(name) ON CONFLICT IGNORE,
            FOREIGN KEY(monster_class_id) REFERENCES monster_class(id)
        )""")
    conn.commit()
    conn.close()


def compile_data():
    # make sure all files exists
    filepath = lib.get_file_path()
    for k in filepath:
        if not os.path.isfile(filepath[k]):
            raise IOError(
                    errno.ENOENT, os.strerror(errno.ENOENT), filepath[k])
    # empty all tables
    conn = init_connection()
    cur = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = cur.fetchall()
    for table in tables:
        cur.execute('DELETE FROM {}'.format(table[0]))

    tech_src = read_csv(filepath['tech'])
    spark_src = read_csv(filepath['spark'])
    unit_src = read_csv(filepath['unit'])
    monster_src = read_csv(filepath['monster'])

    # Starts with type and classes.

    # weapon_type
    # Will never change so hard-coded.
    weapon_type_list = [
        'Sword', 'Great Sword', 'Axe', 'Club',
        'Spears', 'Short Sword', 'Bow', 'Unarmed']
    tup_arg = [(x,) for x in weapon_type_list]
    cur.executemany("""
                INSERT INTO weapon_type(name) values(?)""", tup_arg)
    # spark_type
    # Some adjustment needed to match Character's table.
    # The original still needs to be maintained for spark data referencing.
    # Index needs +1 because SQLite starts at 1, not 0
    spark_type_list = [x for x in spark_src[0][-16:]]
    tup_arg = [(x,) for x in spark_src[0][-16:]]
    # del spark_src[0]  # The header is no longer needed
    cur.executemany("""INSERT INTO spark_type(name) VALUES(?)""", tup_arg)
    rename = [('Polearms (Axe/Spear)', 'Polearm (Axe / Spear)'),
               ('Hammer', 'Club'),
               ('Swordmaster', 'Sword Master'),
               ('1H Swordmaster', '1H Sword Master')
    ]
    cur.executemany("""UPDATE spark_type SET name=? WHERE name=?""", rename)
    # unit_class
    unit_class_list = []
    for u in unit_src[1:]:
        if u[3] not in unit_class_list:
            unit_class_list.append(u[3])
    tup_arg = [(x,) for x in unit_class_list]
    cur.executemany("""INSERT INTO unit_class(name) VALUES(?)""", tup_arg)
    # monster_class
    monster_class_list = []
    for u in monster_src[1:]:
        if u[1] not in monster_class_list:
            monster_class_list.append(u[1])
    tup_arg = [(x,) for x in monster_class_list]
    cur.executemany("""INSERT INTO monster_class(name) VALUES(?)""",  tup_arg)

    # Now the data themselves. ###

    # tech

    # this is 2 fold. 'spark_src' contains all the techs we need.
    # However, it also has weapon names and weapon type mixed up. So we
    # need to get all techs under the right weapon type from 'tech_src' first.
    # Rename list is to take care of naming differences between the 2
    tech_arg = []
    tech_spark_from_arg = []
    spark_tech_pair_arg = []
    tech_dict = {}
    rename = [
        ['Plasma Stab', 'Plasma Thrust'],
        ['Psycho Bind', 'Psycho Shot']
    ]
    for each in tech_src[1:]:
        name = each[0].strip()
        if 'Normal Attack' in name:
            continue
        weapon_type = each[1].strip()
        if weapon_type not in weapon_type_list:
            continue
        tech_dict[name] = weapon_type
    rename2 = [
        ['1H ', ''],
        ['2H ', 'Great '],
        ['Spear', 'Spears'],
        ['Martial Arts', 'Unarmed'],
        ['Shortsword', 'Short Sword'],
    ]
    for each in spark_src[1:]:
        name = each[0].strip()
        for old, new in rename:
            if old in name:
                name = new
        difficulty = each[1].strip()
        from_weapon = each[5]
        for e in rename2:
            from_weapon = from_weapon.replace(e[0], e[1])
        from_weapon = None if from_weapon in weapon_type_list else from_weapon
        if 'Starter' in difficulty:
            continue
        if name in tech_dict:
            tech_arg.append((name, tech_dict[name], from_weapon))
        for spark_type_id, can_spark in enumerate(each[-16:]):
            if can_spark != 'Y':
                continue
            spark_tech_pair_arg.append((spark_type_id + 1, name))
        # spark from
        if each[1] == 'N/A':
            each[1] = None
            tech_spark_from_arg.append((name, None, None))
        else:
            tech_spark_from_arg.append((name, None, each[1]))
        for alts in [each[3], each[4]]:
            if not alts:
                continue
            spark_from, diff = alts.split('(')
            spark_from = spark_from.strip()
            diff = diff.replace(')', '').strip()
            tech_spark_from_arg.append((name, spark_from, diff))
    cur.executemany("""
        INSERT INTO tech(name, weapon_type_id, from_weapon) VALUES(
            ?,(SELECT id FROM weapon_type WHERE name=?), ?)""", tech_arg)
    cur.executemany("""
        INSERT INTO spark_tech_pair(spark_type_id, tech_id)
        VALUES(?, (SELECT id FROM tech WHERE name=?))""", spark_tech_pair_arg)
    cur.executemany("""
        INSERT INTO tech_spark_from(tech_id, spark_from_id, difficulty)
        VALUES(
            (SELECT id FROM tech WHERE name=?),
            (SELECT id FROM tech WHERE name=?),
            ?)""", tech_spark_from_arg)
    # unit
    gerard_count = 0
    tuple_arg = []
    for each in unit_src[1:]:
        unit_name = each[1].strip()
        if unit_name == 'Gerard':
            gerard_count += 1
            if gerard_count == 1:
                unit_name = 'Gerard (armored)'
        unit_class = each[3].strip()
        spark_type = each[9].strip()
        is_female = 0 if each[4] == '\xd7' else 1
        tuple_arg.append( (unit_name, unit_class, spark_type, is_female))
    cur.executemany("""
        INSERT INTO unit(name, unit_class_id, spark_type_id, is_female)
        VALUES( ?,
                (SELECT id FROM unit_class WHERE name=?),
                (SELECT id FROM spark_type WHERE name=?),
                ?)""", tuple_arg)

    # monster
    tuple_arg = []
    Vampire = 0
    for each in monster_src[1:]:
        name = each[0].strip().replace('\xf6', 'o')
        if name == Vampire:
            if not Vampire:
                Vampire += 1
            else:
                name += '2'
        type = each[1].strip()
        tech_level = each[16].strip()
        tuple_arg.append((name, tech_level, type))
    cur.executemany("""
        INSERT INTO monster(name, tech_level, monster_class_id) VALUES(
            ?,?, (SELECT id FROM monster_class WHERE name=?))""", tuple_arg)

    # update various entries to match https://saga.fandom.com/wiki
    unit_rename = [
        ('Gorilla', 'Hedgehog'),
        ('Lisa', 'Liza'),
        ('Tethys', 'Thetis'),
        ('Perusa', 'Pherusa'),
        ('Naushithoe', 'Nausithoe'),
        ('Janice', 'Janis'),
        ('Franklin', 'Franklyn'),
        ('Epiminondas' , 'Epaminondas'),
        ('Zhenghe' , 'Teiwa'),
        ('Sophia', 'Sofia'),
        ('Magadalene', 'Magdalena'),
        ('Georg', 'George'),
        ('Christoph', 'Christophe'),
        ('Numan', 'Neman'),
        ('Abu Hassen', 'Abu Hasan'),
        ('Armanaus', 'Armanus'),
        ('Sulayman', 'Suleiman'),
        ('Doman', 'Duman'),
        ('Nakamaro', 'Nakamalo'),
        ('Seimei', 'Seimay'),
        ('Makibi', 'Maki'),
        ('Hamba', 'Hamuba'),
        ('Kawaa', 'Kuwawa'),
        ('Vai', 'Bai'),
        ('Cloudia', 'Claudia'),
        ('Gaile', 'Gail'),
        ('Eire', 'Air'),
        ('Stormie', 'Stormy'),
        ('Nadire', 'Nadir'),
        ('Ganryu', 'Ryu'),
        ('Souji', 'Soji'),
        ('Ivy', 'Ivey'),
        ('Windseed', 'Seed'),
        ('Hippolyte', 'Hippolyta'),
        ('Deidamia', 'Deidameia'),
        ('Seiro', 'Cielo'),
        ('Rego', 'Lego'),
        ('Cherno', 'Cheruno'),
        ('Pod', 'Podo'),
        ('Gley', 'Gurai'),
        ('Bruni', 'Buruni'),
        ('Vertisol', 'Bertie'),
        ('Panelope', 'Penelope'),
        ('Besma', 'Besuma'),
        ('Alia', 'Aria'),
        ('Mirza', 'Mizura'),
        ('Azizah', 'Azuiza'),
        ('Dunya', 'Doniya'),
        ('Nuzat', 'Nozuhatou'),
        ('Shahrazad', 'Shaharazado'),
        ('Galdan', 'Regal'),
        ('Ezen', 'Esen'),
        ('Agunda', 'Akuda'),
        ('Abaoji', 'Aboki'),
        ('Modu', 'Bokutotsu'),
        ('Marie', 'Mary'),
        ('Julianna', 'Juliana'),
        ('Philippe', 'Philip'),
        ('Aleksandr', 'Alexander'),
        ('Fredrich', 'Friedrich'),
        ('Harfagre', 'Harf-Agre'),
        ('Sigurdr', 'Sigurd'),
        ('Kelut', 'Kelud'),
        ('Galunggung', 'Galuggang'),
        ('Awe', 'Awu'),
        ('Merapi', 'Marapi'),
        ('Papandjar', 'Papandayan'),
        ('Shiyuan', 'Shigen'),
        ('Boyue', 'Yak'),
        ('Shanfu', 'Tanpuku'),
        ('Mengde', 'Moutoku'),
        ('Ferret', 'Ferrets'),
        ('Atlan', 'Altan')
    ]
    cur.executemany("""UPDATE unit SET name=? WHERE name=?""", unit_rename)
    monster_rename = [
        ('Nixie', 'Nixer'),
        ('Peg Powler', 'Peg Prowler'),
        ('Cyfreet', 'Siefried'),
        ('Doppelganger', 'Mimic'),
        ('Garon', 'Gallon'),
        ('Narwhale', 'Sea Lord'),
        ('Water Faerie', 'Water Fairy'),
        ('Earth Faerie', 'Earth Fairy'),
        ('Fire Faerie', 'Fire Fairy'),
        ('Wind Faerie', 'Wind Fairy'),
        ('Sidhe', 'Shea'),
        ('Despoiler', 'Depsoiler'),
        ('Faerie_Bug', 'Fairie Bug'),
        ('Termite Soldier', 'Formian Soldier'),
        ('Lizardman', 'Lizardfolk'),
        ('Lizardlady', 'Lizard Warrior'),
        ('Werefrog', 'Ueafuroggu'),
        ('Tuatara', 'Turtulla'),
        ('Alligator', 'Wanigeta'),
        ('Fusion Slug', 'Fusion Beast'),
        ('Poisan', 'Poison'),
        ('Gormenghast', 'Gomengasuto'),
        ('Sahra', 'Sarah'),
        ('Skull Lord', 'Skull Rod'),
        ('Mobelwagen (Aquatic)', 'Mbelwagen (Aquatic)'),
        ('Battle Ogre', 'Ogre Warrior'),
        ('Sephira', 'Saphirah')
    ]
    cur.executemany("""UPDATE monster SET name=? WHERE name=?""", monster_rename)

    conn.commit()
    conn.close()


def read_csv(csv_file):
    result = []
    with open(csv_file) as f:
        data = csv.reader(f)
        for row in data:
            result.append([t.replace('\xa0', '') for t in row])
    return result


if __name__ == '__main__':
    # create the database file if it doesn't exist.
    if not os.path.isfile(filepath['sqlite']):
        create_table(filepath['sqlite'])
    compile_data()