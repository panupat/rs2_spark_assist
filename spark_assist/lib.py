import os


def get_file_path():
    filepath = {}
    current_path = os.path.dirname(os.path.realpath(__file__))
    filepath['sqlite'] = os.path.join(current_path, 'database.sqlite')
    filepath['tech'] = os.path.join(current_path, 'csv', 'tech.csv')
    filepath['spark'] = os.path.join(current_path, 'csv', 'spark.csv')
    filepath['unit'] = os.path.join(current_path, 'csv', 'unit.csv')
    filepath['monster'] = os.path.join(current_path, 'csv', 'monster.csv')
    return filepath