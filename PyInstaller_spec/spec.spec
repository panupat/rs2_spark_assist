# -*- mode: python -*-

block_cipher = None


a = Analysis(['C:/script/rs2_spark_assist/rs2_spark_assist/spark_assist/spark_assist_app.py'],
             pathex=['C:/script/rs2_spark_assist/rs2_spark_assist/'],
             binaries=[],
             datas=[('C:/script/rs2_spark_assist/rs2_spark_assist/spark_assist/database.sqlite', '.'), ('C:/script/rs2_spark_assist/rs2_spark_assist/spark_assist/lib.py', '.'), ('C:/script/rs2_spark_assist/rs2_spark_assist/spark_assist/spark_assist_ui.py', '.'), ('C:/script/rs2_spark_assist/rs2_spark_assist/spark_assist/resource.py', '.')],
             hiddenimports=['PySide.QtCore', 'PySide.QtGui', 'qdarkstyle'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='spark_assist_app',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
