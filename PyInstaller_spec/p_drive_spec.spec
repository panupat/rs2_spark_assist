# -*- mode: python -*-

block_cipher = None


a = Analysis(['P:/script/wip/rs2_spark_assist/spark_assist/spark_assist_app.py'],
             pathex=['P:/script/wip/rs2_spark_assist/'],
             binaries=[],
             datas=[('P:/script/wip/rs2_spark_assist/spark_assist/database.sqlite', '.'), ('P:/script/wip/rs2_spark_assist/spark_assist/lib.py', '.'), ('P:/script/wip/rs2_spark_assist/spark_assist/spark_assist_ui.py', '.'), ('P:/script/wip/rs2_spark_assist/spark_assist/resource.py', '.')],
             hiddenimports=['PySide.QtCore', 'PySide.QtGui', 'qdarkstyle'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='spark_assist_app',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False,
          icon='P:/script/wip/rs2_spark_assist/gerard.ico')
