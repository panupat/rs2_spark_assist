# rs2_spark_assist

Romancing Saga 2 tech spark assistance tool

## Usage:

*spark_assist_app.py* is the main application. It can be run as is with the database and images provide.

*Requirements: Python 2.7, PySide, qdarkstyle.*

*initialize_database.py* converts CSV into Sqlite database, along with all necessary adjustments.

## Windows executable (.exe)

Can be downloaded from my [Google Drive](https://drive.google.com/drive/folders/1P-TGaKjGY_kxGqeswHFKQx1j3-cY9gAT)

## CSV preparation

Source of CSV are [Google spreadsheets compiled by Elliot20](https://gamefaqs.gamespot.com/boards/588633-romancing-saga-2/76134565).

Follow links to spreadsheets, copy all contents, paste into local spreadsheets application such as Excel or LibreOffice. Save as text CSV into *csv* subdirectory.

[Character Data](https://tinyurl.com/y952wn6q) >> unit.csv

[Monster Data](https://tinyurl.com/ycuw2h55) >> monster.csv

[Tech Data](https://tinyurl.com/y7p8ucz3) >> tech.csv

[Tech Spark Data](https://tinyurl.com/ycc8u54x) >> spark.csv

Run *initialize_database.py* to purge all old data and generate everything from scratch.

## Special thanks

Elliot for all the [data sheets](https://gamefaqs.gamespot.com/boards/588633-romancing-saga-2/76134565).

Falconpunch for [character sprites](https://www.vg-resource.com/thread-33412.html)

Feral13's [tech tree](http://farelforever.com/tripletriad/techtree.html) which inspired this script :) 

## Version history

- 0.4.1 Make character class order alphabetically.
- 0.4 All character sprites completed. Fix empty monster tree item loading without image. 
- 0.3.1 Added more sprites.
- 0.3 Test release. Many character sprites still need to be named.
- 0.2 First working prototype. 
- 0.1 Early skeleton